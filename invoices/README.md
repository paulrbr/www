# hetzner-invoice-summary

_Taken from https://github.com/mope1/hetzner-invoice-summary_

## Make hetzner invoice CSV files human readable

If you scale an application on hetzner cloud by starting and destroying servers, you get very messy bills. This tiny script solves that problem.

1. Download CSV files from https://accounts.hetzner.com/invoice
2. Put them in `invoices-csv` subfolder
3. Run script with pyhon3
