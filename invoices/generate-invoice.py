import glob
import csv
import sys

def extract_name(description):
  name = description
  if '""' in description:
    name = description.split('""')[1]
  return name

summary_csv_writer = csv.writer(sys.stdout)
summary_csv_writer.writerow(['month', 'type', 'name', 'amount', 'cost'])

invoices_path='**/Hetzner_*.csv'
invoice_files = glob.glob(invoices_path)
invoice_files.sort()
for filename in invoice_files:
  with open(filename, 'r') as csvfile:
    csv_text = csvfile.read()
    month = None

    servers={}
    backups={}
    snapshots={}
    block_storages={}
    floating_ips = { 'count': 0, 'cost': 0 }
    ips = { 'count': 0, 'cost': 0 }

    # python3 stdlib csv package does not handle newlines in column
    # doing the parsing manually
    # HACK assuming last column is empty string
    for row in csv_text.split(',""\n'):
      # HACK assuming columns are all strings
      columns = row.split('","')
      index_shift = 0
      if "Cloud Project" in columns[0]:
        index_shift = 1
      columns[index_shift + 1] = columns[index_shift + 1].split('\n') # split by newline in-place
      columns[index_shift + 6] = columns[index_shift + 6][:-1] # Remove remaining quote in last column
      #print(columns)

      if month is None:
        month = columns[index_shift +  2][:-3]

      if 'Server' in columns[index_shift + 0]:
        hostname = extract_name(columns[index_shift + 1][0])
        usage_hours = float(columns[index_shift + 4])
        cost=float(columns[index_shift + 5])*usage_hours
        price=float(columns[index_shift + 6])

        if (round(cost, 3) - round(price, 3)) != 0.0:
          print('Cost (quantity*unit price): '+ str(cost) + ' is not equal to Hetzner total Price: '+ str(price), file = sys.stderr)

        if hostname not in servers:
          servers[hostname] = {
            'usage_hours': usage_hours,
            'cost': cost,
          }

        else:
          servers[hostname]['usage_hours']+=usage_hours
          servers[hostname]['cost']+=cost

      elif 'Snapshot' in columns[index_shift + 0]:
        snapname = extract_name(columns[index_shift + 1][0])
        usage_gbs = float(columns[index_shift + 4])
        cost=float(columns[index_shift + 5])*usage_gbs
        price=float(columns[index_shift + 6])

        if (round(cost, 3) - round(price, 3)) != 0.0:
          print('Cost (quantity*unit price): '+ str(cost) + ' is not equal to Hetzner total Price: '+ str(price), file = sys.stderr)

        if snapname not in snapshots:
          snapshots[snapname] = {
            'usage_gbs': usage_gbs,
            'cost': cost,
          }

        else:
          snapshots[snapname]['usage_gb']+=usage_gb
          snapshots[snapname]['cost']+=cost

      elif 'Block storage' in columns[index_shift + 0]:
        storage_name = extract_name(columns[index_shift + 1][0])
        gb_months = float(columns[index_shift + 4])
        cost=float(columns[index_shift + 5])*gb_months
        price=float(columns[index_shift + 6])

        if (round(cost, 3) - round(price, 3)) != 0.0:
          print('Cost (quantity*unit price): '+ str(cost) + ' is not equal to Hetzner total Price: '+ str(price), file = sys.stderr)

        if storage_name not in block_storages:
          block_storages[storage_name] = {
            'gb_months': gb_months,
            'cost': cost,
          }

        else:
          block_storages[storage_name]['gb_months']+=gb_months
          block_storages[storage_name]['cost']+=cost

      elif 'Floating IP' in columns[index_shift + 0]:
        count = float(columns[index_shift + 4])
        cost=float(columns[index_shift + 5])*count
        price=float(columns[index_shift + 6])

        if (round(cost, 3) - round(price, 3)) != 0.0:
          print('Cost (quantity*unit price): '+ str(cost) + ' is not equal to Hetzner total Price: '+ str(price), file = sys.stderr)

        floating_ips['count']+=count
        floating_ips['cost']+=cost

      elif 'IP' in columns[index_shift + 0]:
        count = float(columns[index_shift + 4])
        cost=float(columns[index_shift + 5])*count
        price=float(columns[index_shift + 6])

        if (round(cost, 3) - round(price, 3)) != 0.0:
          print('Cost (quantity*unit price): '+ str(cost) + ' is not equal to Hetzner total Price: '+ str(price), file = sys.stderr)

        ips['count']+=count
        ips['cost']+=cost

      elif 'Backup' in columns[index_shift + 0]:
        hostname = extract_name(columns[index_shift + 1][0])
        usage_hours = float(columns[index_shift + 4])
        cost=float(columns[index_shift + 5])*usage_hours
        price=float(columns[index_shift + 6])

        if (round(cost, 3) - round(price, 3)) != 0.0:
          print('Cost (quantity*unit price): '+ str(cost) + ' is not equal to Hetzner total Price: '+ str(price), file = sys.stderr)

        if hostname not in backups:
          backups[hostname] = {
            'usage_hours': usage_hours,
            'cost': cost,
          }

        else:
          backups[hostname]['usage_hours']+=usage_hours
          backups[hostname]['cost']+=cost

      else:
        print('Unhandled entry: '+ str(columns), file = sys.stderr)

    for hostname, summary in servers.items():
      summary_csv_writer.writerow([month, 'server', hostname, summary['usage_hours'], round(summary['cost'], 3) ])

    for backup, summary in backups.items():
      summary_csv_writer.writerow([month, 'backup', backup, backups[backup]['usage_hours'], round(backups[backup]['cost'], 3) ])

    for snapname, summary in snapshots.items():
      summary_csv_writer.writerow([month, 'snapshot', snapname, summary['usage_gbs'], round(summary['cost'], 3) ])

    for snapname, summary in block_storages.items():
      summary_csv_writer.writerow([month, 'block_storage', snapname, summary['gb_months'], round(summary['cost'], 3) ])

    if floating_ips['count'] > 0:
      summary_csv_writer.writerow([month, 'floating_ips', 'all', floating_ips['count'], round(floating_ips['cost'], 3) ])

    if ips['count'] > 0:
      summary_csv_writer.writerow([month, 'ips', 'all', ips['count'], round(ips['cost'], 3) ])
