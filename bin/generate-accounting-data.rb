#!/usr/bin/env ruby

require "date"
require "csv"
begin
  require "terminal-table"
rescue LoadError
  warn "WARNING: needs ruby dependency 'gem install terminal-table'"
end

class Transaction
  attr_accessor :cents, :date

  def initialize(cents:, date:, date_format: "%Y-%m")
    @cents = cents
    @date = Date.strptime(date.to_s, date_format)
  end

  def credit?
    cents.positive?
  end

  def debit?
    cents.negative?
  end
end

class Runner
  HETZNER_INVOICES = "invoices/hetzner_invoices.csv"
  GANDI_INVOICES = "invoices/gandi_invoices.csv"
  LIBERAPAY_DONATIONS = "credits/liberapay_donations.csv"

  attr_reader :transactions

  def initialize
    @transactions = []

    # Load hetzner data
    CSV.foreach(HETZNER_INVOICES, "r", converters: %i[numeric], headers: :first_row) do |row|
      cents_ht = (row[4] * -100).to_i
      cents_ttc = cents_ht * 1.2
      transactions << Transaction.new(cents: cents_ttc, date: row[0])
    end

    # Load gandi data
    CSV.foreach(GANDI_INVOICES, "r", converters: %i[date numeric], headers: :first_row) do |row|
      cents = (row[1] * -100).to_i
      transactions << Transaction.new(cents: cents, date: row[0], date_format: "%Y-%m-%d")
    end

    # Load liberapay data
    CSV.foreach(LIBERAPAY_DONATIONS, "r", converters: %i[numeric], headers: :first_row) do |row|
      transactions << Transaction.new(cents: row[1], date: row[2], date_format: "%s")
    end
  end

  def run
    table_rows = []
    monthly_balances = transactions.group_by { |transaction|
      [transaction.date.year, transaction.date.month.to_s.rjust(2, "0")]
    }.sort.map { |month, transactions|
      month_str = month.join("-")
      total_credit = (transactions.select(&:credit?).map(&:cents).sum / 100.0).round(2)
      total_debit = (transactions.select(&:debit?).map(&:cents).sum / 100.0).round(2)
      total = (total_credit + total_debit).round(2)

      [month_str, total_debit, total_credit, total]
    }

    monthly_balances.each do |month, total_debit, total_credit, total|
      puts "Balance au #{month} de #{total} €"
      table_rows << [month, "#{total_debit} €", "#{total_credit} €", "#{total} €"]
    end

    puts ""
    table_rows << :separator

    balance_debits = monthly_balances.sum { |_, total_debit| total_debit }
    balance_credits = monthly_balances.sum { |_, _, total_credit| total_credit }
    balance = monthly_balances.sum { |_, _, _, total| total }

    puts "Balance total au #{Time.now.to_date} de #{balance} €"
    table_rows << ["Total (au #{Time.now.to_date})", "#{balance_debits} €", "#{balance_credits} €" , "#{balance} €"]

    begin
      puts ""
      puts ::Terminal::Table.new headings: ['Mois', 'Debits', 'Credits', 'Balance'], rows: table_rows
    rescue NameError
      warn "WARNING: needs ruby dependency 'gem install terminal-table'"
      warn "WARNING: to generate ASCII table"
    end
  end
end

Runner.new.run
