import os
import stripe
import csv
import re
from datetime import datetime

dir_path = os.path.dirname(os.path.realpath(__file__))

def search_charge(dict_reader, charge):
    for row in dict_reader:
        if row["id"] == charge["id"]:
            return 1
    return -1

stripe.api_key = os.environ["STRIPE_API_KEY"]
destination_file = os.path.join(dir_path, 'liberapay_donations.csv')
charges = stripe.Charge.list(limit=100).data

# Read existing file
existing_fieldnames = None
existing_charges = list()
writes = 0

try:
    with open(destination_file, 'r') as f:
        csv_reader = csv.DictReader(f)
        existing_fieldnames = csv_reader.fieldnames
        for row in csv_reader:
            existing_charges.append(row["id"])
except FileNotFoundError:
    print("File doesn't exist yet...")

# now we will open a file for writing
write_data_file = open(destination_file, 'a')
fieldnames = ["id", "amount", "created"]
csv_writer = csv.DictWriter(write_data_file, fieldnames=fieldnames)

# Write CSV header if file is empty
if not bool(existing_fieldnames):
    csv_writer.writeheader()

for charge in charges:
    if not charge["id"] in existing_charges and charge["captured"] and re.match(r"liberapay", charge["source"]["application_name"], re.I):
        writes = writes + 1
        csv_writer.writerow({ key: charge[key] for key in ["id", "amount", "created"] })
                            # [charge["id"], charge["amount"], datetime.utcfromtimestamp(int(charge["created"])).strftime('%Y-%m-%d %H:%M:%S')])

if writes > 0:
    print(writes, "charges written to", destination_file)
else:
    print("Nothing to do")
