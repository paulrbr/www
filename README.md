# www.interfoodcoop.net - Site web du collectif

Interfoodcoop est un collectif monté dans la communauté intercoop entre plusieurs [supermarchés coopératifs](https://forum.supermarches-cooperatifs.fr/) de France dont le but est de fournir à la fois des outils et de développer des logiciels open-source pour le bien commun des supermarchés.

Ce site web a été (très) fortement inspiré par celui de [InterHop](https://framagit.org/interchu/interhop-website) et centralise les informations liés aux travaux communs fait pour les supermarchés coopératifs de France.

## Usage

Install the dependencies with [Bundler](https://bundler.io/):

~~~bash
$ make install
~~~

Run `jekyll` locally to serve the website:

~~~bash
$ make serve
~~~


# More 

## Jekyll

- Cheatsheet : https://devhints.io/jekyll
- Mutltilingual : https://www.sylvaindurand.org/making-jekyll-multilingual/
- Scope : https://jekyllrb.com/docs/configuration/front-matter-defaults/
- Permalink and Slug : https://jekyllrb.com/docs/permalinks/
- redirect Module : https://github.com/jekyll/jekyll-redirect-from
- Collections : https://jekyllrb.com/docs/collections/

## Pagination Multi Language

- https://webniyom.com/jekyll-dual-language/
- example : https://github.com/bradonomics/jekyll-dual-language

## Online svg converter

- https://image.online-convert.com/fr/convertir-en-svg
- https://www.pngtosvg.com

## Online pixel converter

- http://convert-my-image.com/ImageConverter_Fr

## Free icons

- https://www.flaticon.com

## gitlab pages deploiment

- https://framacolibri.org/t/gitlab-page-explication-derreur-pipeline/5852/5
- https://docs.framasoft.org/fr/gitlab/gitlab-pages.html
- https://blog.cloudflare.com/secure-and-fast-github-pages-with-cloudflare/
