module Jekyll
  class SectionTag < Liquid::Block
    def initialize(tag_name, input, tokens)
      super
      @input = input
    end

    def render(context)
      section_type, col_type, float_type = @input.split('|').map(&:strip)
      case float_type
      when 'left'
        left_div = ""
        right_div = "<div></div>"
      when 'right'
        left_div = "<div></div>"
        right_div = ""
      else
        left_div = ""
        right_div = ""
      end

      if float_type == 'custom'
        content = super
      else
        content = <<~TXT
          <div markdown="1">
          #{super}
          </div>
        TXT
      end

      <<~OUT
        <section class="diagonal #{section_type}">
          <div class="container #{col_type}">
            #{left_div}
        #{content}
            #{right_div}
          </div>
        </section>
      OUT
    end
  end
end

Liquid::Template.register_tag('section', Jekyll::SectionTag)
