JEKYLL_ENV ?= development

.PHONY: install
install:
	gem install bundler
ifeq ($(JEKYLL_ENV), production)
	bundle config set without 'development'
endif
	bundle install

.PHONY: build
build:
	bundle exec jekyll build -d public

.PHONY: serve
serve:
	bundle exec jekyll serve --trace

invoices/hetzner_invoices.csv:
	@echo
	python3 invoices/generate-invoice.py > $@

credits/liberapay_donations.csv:
	@echo
	python3 credits/fetch-all.py

credits/gandi_invoices.csv:

.PHONY: accounting-deps
accounting-deps:
	apt update && apt install -y python3-pip
	pip3 install -r credits/requirements.txt
	gem install terminal-table

.PHONY: accounting
accounting: accounting-deps invoices/hetzner_invoices.csv credits/liberapay_donations.csv credits/gandi_invoices.csv
	@echo
	./bin/generate-accounting-data.rb
